package com.umis.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.umis.pojo.Student;

@Mapper
public interface StudentMapper {
	public Student findByXuehao(String studentId);
}
