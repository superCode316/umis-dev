package com.umis.mapper;

import org.apache.ibatis.annotations.Mapper;

/**
 * @author: 钱曹宇
 * @date: 2019/4/2 3:24 PM
 * @description:
 */

@Mapper
public interface AccessTokenMapper {

    public String getAccessToken();

    public void saveAccessToken(String accessToken);

}
