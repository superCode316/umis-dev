package com.umis.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.umis.pojo.TemplateData;
import com.umis.pojo.TemplateMessage;
import com.umis.utils.WeixinUtils;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: 钱曹宇
 * @date: 2019/4/3 9:11 PM
 * @description:
 */
@Service
public class SendMessageServiceImpl implements SendMessageService {


    private AccessTokenService accessTokenService;


    /**
     *
     * @param appId 服务号的appid
     * @param appSecret 服务号的appsecret
     * @param openId 需要发送对象的openid
     * @return 返回成功值
     * @throws Exception 抛出所有异常
     */
    @Override
    public int sendTemplateMessage(String appId, String appSecret, String openId) throws Exception{


        /** 初始化json工具 */
        ObjectMapper mapper = new ObjectMapper();

        /** 开始创建消息 */
        TemplateMessage message = new TemplateMessage();
        // 设置跳转的url
        message.setUrl("https://www.google.com");
        // 设置需要发送的用户openid
        message.setTouser(openId);
        // 设置消息模版
        message.setTemplate_id("0xWBrzvxIptMlhOAwHNABUUBXMRfvetgbMgxtM_WVsY");
        // 设置消息标题的颜色
        message.setTitle_color("#000000");

        // 设置消息的body
        Map<String,TemplateData> map = new HashMap<String, TemplateData>();
        /** 第一条 */
        TemplateData data1 = new TemplateData();
        //第一条body的颜色
        data1.setColor("#000000");
        //第一条body
        data1.setValue("你有一条新的消息");
        map.put("data1",data1);
        //把body塞进消息
        message.setData(map);

        /** 将消息转化为json */
        String param = mapper.writeValueAsString(message);
        System.out.println(param);
        /** 调用WeixinUtils发送信息 */
        return WeixinUtils.sendTemplateMessage(appId,appSecret,param);

    }
}
