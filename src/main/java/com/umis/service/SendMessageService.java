package com.umis.service;

/**
 * @author: 钱曹宇
 * @date: 2019/4/3 9:15 PM
 * @description:
 */
public interface SendMessageService {

    public int sendTemplateMessage(String appId, String appSecret, String openId) throws Exception;

}
