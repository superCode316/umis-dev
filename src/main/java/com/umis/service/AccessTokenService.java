package com.umis.service;


/**
 * @author: 钱曹宇
 * @date: 2019/4/2 3:22 PM
 * @description:
 */
public interface AccessTokenService {

    public void saveAccessToken(String accessToken);

    public String getAccessToken();

}
