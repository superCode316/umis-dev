package com.umis.service;

import com.umis.mapper.AccessTokenMapper;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author: 钱曹宇
 * @date: 2019/4/2 3:21 PM
 * @description:
 */
public class AccessTokenServiceImpl implements AccessTokenService{

    @Autowired
    private AccessTokenMapper accessTokenMapper;

    public String getAccessToken(){
        return accessTokenMapper.getAccessToken();
    }

    public void saveAccessToken(String token){
        accessTokenMapper.saveAccessToken(token);
    }

}
