package com.umis.controller;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class LoginController {

	
	@RequestMapping("/toLogin")
	public String toLogin() {
		return "/login";
	}
	
	@RequestMapping("/success")
	public String success() {
		System.out.println("success");
		return "/success";
	}
	
	@RequestMapping("/login")
	public String login(String studentId,String password,Model model) {
		Subject subject = SecurityUtils.getSubject();
		System.out.println(studentId +"   "+ password);
		UsernamePasswordToken token = new UsernamePasswordToken(studentId,password);
		try{
			subject.login(token);
			return "/success";
		}catch(UnknownAccountException e) {
			model.addAttribute("msg", "输入的学号不正确，请重新输入");
			return "/login";
		}catch(IncorrectCredentialsException e) {
			model.addAttribute("msg","输入的密码不正确，请重新输入");
			return "/login";
		}
	}
	
	@RequestMapping("/noAuth")
	public String noAuth() {
		return "/noAuth";
	}
	
}
