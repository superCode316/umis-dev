package com.umis.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.umis.pojo.TemplateData;
import com.umis.pojo.TemplateMessage;
import com.umis.service.AccessTokenService;
import com.umis.utils.WeixinUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static com.umis.utils.WeixinUtils.sha1;
import static com.umis.utils.WeixinUtils.sort;

/**
 * @author: 钱曹宇
 * @date: 2019/4/3 7:31 AM
 * @description:
 */

@RestController
@RequestMapping("/wechat/api")
public class WechatController {

    private AccessTokenService accessTokenService;

    private Logger logger = LoggerFactory.getLogger(getClass());

    // URL:   http://www.xxxx.com/wechat/api/authentication
    // Token: 此处TOKEN即为微信接口配置信息的Token

    private String TOKEN = "wechat";

    /**
     * 验证微信后台配置的服务器地址有效性
     *
     * 接收并校验四个请求参数
     *
     * @param signature 微信加密签名
     * @param timestamp 时间戳
     * @param nonce     随机数
     * @param echostr   随机字符串
     * @return echostr
     */
    @GetMapping("/authentication")
    public String checkName(@RequestParam(name = "signature") String signature,
                            @RequestParam(name = "timestamp") String timestamp,
                            @RequestParam(name = "nonce") String nonce,
                            @RequestParam(name = "echostr") String echostr) {

        logger.info("微信-开始校验签名");
        logger.info("收到来自微信的 echostr 字符串:{}", echostr);

//        加密/校验流程如下:
//        1. 将token、timestamp、nonce三个参数进行字典序排序
//        2. 将三个参数字符串拼接成一个字符串进行sha1加密
//        3. 开发者获得加密后的字符串可与signature对比，标识该请求来源于微信

        // 1.排序
        String sortString = sort(TOKEN, timestamp, nonce);
        // 2.sha1加密
        String myString = sha1(sortString);
        // 3.字符串校验
        if (!(myString.equals(""))&& myString != null && myString.equals(signature)) {
            logger.info("微信-签名校验通过");
            //如果检验成功原样返回echostr，微信服务器接收到此输出，才会确认检验完成。
            logger.info("回复给微信的 echostr 字符串:{}", echostr);
            return echostr;
        } else {
            logger.error("微信-签名校验失败");
            return "";
        }
    }


    /*
    <xml>
      <ToUserName><![CDATA[toUser]]></ToUserName>
      <FromUserName><![CDATA[FromUser]]></FromUserName>
      <CreateTime>123456789</CreateTime>
      <MsgType><![CDATA[event]]></MsgType>
      <Event><![CDATA[subscribe]]></Event>
    </xml>
     */
    //具体文档参考https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421140454
    /**
     * 微信服务器自动推送消息
     */
    @PostMapping("/from-weixin/get-message")
    private void getMessageFromWeixin(){

    }


}
