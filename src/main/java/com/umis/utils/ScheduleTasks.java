//package com.umis.utils;
//
//import com.umis.service.AccessTokenService;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.boot.CommandLineRunner;
//import org.springframework.core.annotation.Order;
//import org.springframework.stereotype.Component;
//
//import java.lang.reflect.GenericArrayType;
//import java.util.concurrent.ExecutorService;
//import java.util.concurrent.Executors;
//import java.util.concurrent.ScheduledExecutorService;
//import java.util.concurrent.TimeUnit;
//
///**
// * @author: 钱曹宇
// * @date: 2019/4/2 12:55 AM
// * @description: 定时任务类 实现CommandLineRunner接口 注解使得在运行任务的时候就开始定时任务
// */
//@Component
//@Order(value = 1)
//public class ScheduleTasks implements CommandLineRunner,Runnable {
//
//    private final int GET_TOKEN_TIME = 3600000;
//    private AccessTokenService accessTokenService;
//    private ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(2);
//    private Logger logger = LoggerFactory.getLogger(getClass());
//    //    @Value("${weixin.appid}")
//    private String appId = "wxbc7b7ffb371c2ad1";
//    //    @Value("${weixin.secret}")
//    private String secret = "e4874ab0747c7b87afbcd6cfabc6e3e7";
//    /**
//     * 进行定时任务
//     * 每隔一个小时到微信服务器获取access_token
//     * @throws Exception 抛出所有异常
//     */
//    @Override
//    public void run(String... strings) throws Exception{
//        ExecutorService executor = Executors.newFixedThreadPool(2);
//        executor.execute(this);
//    }
//
//
//    @Override
//    public void run() {
//        try {
//            //保存token
//            String token = WeixinUtils.getAccessToken(appId,secret);
//            accessTokenService.saveAccessToken(token);
//            scheduledExecutorService.schedule(this, GET_TOKEN_TIME, TimeUnit.MILLISECONDS);
//            logger.info("Access_token has been obtained from weixin server and saved to database");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//}
