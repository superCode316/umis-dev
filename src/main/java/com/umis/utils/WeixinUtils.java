package com.umis.utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.umis.service.AccessTokenService;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

/**
 * @author: 钱曹宇
 * @date: 2019/4/1 11:35 PM
 * @description: 微信的工具
 */
@Component
public class WeixinUtils {

    private static AccessTokenService accessTokenService;

    /**
     * 获取AccessToken
     * @param appId appid
     * @param secret secret
     * @return 从微信服务器获取的access_token
     * @throws Exception
     */
    public static String getAccessToken(String appId, String secret) throws Exception {

        StringBuilder builder = new StringBuilder();
        BufferedReader reader = null;
        String getTokenUrl = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" + appId + "&secret=" + secret;
        URL url = new URL(getTokenUrl);
        //打开url链接
        URLConnection connection = url.openConnection();
        connection.connect();
        reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String line;
        while ((line = reader.readLine()) != null) {
            builder.append(line);
        }
        if (reader != null) {
            reader.close();
        }
        String data = builder.toString();
        ObjectMapper mapper = new ObjectMapper();

        JsonNode node = mapper.readTree(data);
        String token = node.get("access_token").asText();

        return token;

    }

    /**
     * 发送模版消息
     * @param appId appid
     * @param secret secret
     * @param param 消息的内容
     * @return 错误码，若为0则发送成功
     * @throws Exception
     */
    public static int sendTemplateMessage(String appId, String secret,String param) throws Exception{

        StringBuilder builder = new StringBuilder();
        BufferedReader reader = null;
        PrintWriter out = null;
//        String accessToken = accessTokenService.getAccessToken();
        String accessToken = "20_nGcX-g7vPWiOacOuq6ZGQ9kjrcUnNhVL57Ccd6K7tfQCu7aFE1orIoQoliMwj9-PKalDJMCRBuiAHajFSFL95hEo-Sn6yHBjdwQ--nHyrFho-TPevyhiV1vugqcPOMeAFASRP";
        String sendTemplateMessageUrl = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=" + accessToken;
        URL url = new URL(sendTemplateMessageUrl);
        //打开url链接
        URLConnection connection = url.openConnection();
        //发送POST请求
        connection.setDoOutput(true);
        connection.setDoInput(true);
        out = new PrintWriter(connection.getOutputStream());
        out.print(param);
        out.flush();
        reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String line;
        while ((line = reader.readLine()) != null) {
            builder.append(line);
        }
        if (reader != null) {
            reader.close();
        }
        if (out != null){
            out.close();
        }

        String data = builder.toString();
        ObjectMapper mapper = new ObjectMapper();
        JsonNode node = mapper.readTree(data);
        String errCode = node.get("errcode").asText();
        System.out.println(node.asText());
        return Integer.parseInt(errCode);




//        //编码格式
//
//        String charset = "UTF-8";
//
//        //使用帮助类HttpClients创建CloseableHttpClient对象.
//
//        CloseableHttpClient client = HttpClients.createDefault();
//
//        //HTTP请求类型创建HttpPost实例
//
//        HttpPost post = new HttpPost(sendTemplateMessageUrl);
//
//        //使用addHeader方法添加请求头部,诸如User-Agent, Accept-Encoding等参数.
//
//        post.setHeader("Content-Type", "application/json;charset=UTF-8");
//
//        //组织数据
//        StringEntity se = new StringEntity(param);
//
//        //设置编码格式
//
//        se.setContentEncoding(charset);
//
//        //设置数据类型
//
//        se.setContentType("application/json");
//
//        //对于POST请求,把请求体填充进HttpPost实体.
//
//        post.setEntity(se);
//
//        //通过执行HttpPost请求获取CloseableHttpResponse实例 ,从此CloseableHttpResponse实例中获取状态码,错误信息,以及响应页面等等.
//
//        CloseableHttpResponse response = client.execute(post);
//
//        //通过HttpResponse接口的getEntity方法返回响应信息，并进行相应的处理 
//
//        HttpEntity entity = response.getEntity();
//
//        String resData = EntityUtils.toString(response.getEntity());
//        JsonNode nodes = mapper.readTree(data);
//        System.out.println(nodes.asText());
//        return 0;


    }

    /**
     * 排序方法
     * @param token     Token
     * @param timestamp 时间戳
     * @param nonce     随机数
     * @return
     */
    public static String sort(String token, String timestamp, String nonce) {
        String[] strArray = {token, timestamp, nonce};
        Arrays.sort(strArray);
        StringBuilder sb = new StringBuilder();
        for (String str : strArray) {
            sb.append(str);
        }

        return sb.toString();
    }

    /**
     * 将字符串进行sha1加密
     *
     * @param str 需要加密的字符串
     * @return    加密后的内容
     */
    public static String sha1(String str) {
        try {
            System.out.println("123123");
            MessageDigest digest = MessageDigest.getInstance("SHA-1");
            digest.update(str.getBytes());
            byte messageDigest[] = digest.digest();
            // 创建 16进制字符串
            StringBuffer hexString = new StringBuffer();
            // 字节数组转换为 十六进制 数
            for (int i = 0; i < messageDigest.length; i++) {
                String shaHex = Integer.toHexString(messageDigest[i] & 0xFF);
                if (shaHex.length() < 2) {
                    hexString.append(0);
                }
                hexString.append(shaHex);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

}

