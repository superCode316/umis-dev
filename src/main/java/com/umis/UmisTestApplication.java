package com.umis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class UmisTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(UmisTestApplication.class, args);
	}

}
