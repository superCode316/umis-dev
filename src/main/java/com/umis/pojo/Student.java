package com.umis.pojo;

import java.util.Date;

import lombok.Data;

/*
 * 
 * 
 * xuehao` varchar(8) NOT NULL,
  `banjihao` varchar(8) DEFAULT NULL,
  `shifoupinkunsheng` varchar(1) DEFAULT NULL,
  `zhengzhimianmao` varchar(10) DEFAULT NULL,
  `shifouxiaowaizhusu` varchar(1) DEFAULT NULL,
  `xuejizhuangtai` varchar(10) DEFAULT NULL,
  `xingming` varchar(4) NOT NULL,
  `xingbie` varchar(1) NOT NULL,
  `chushengriqi` date NOT NULL,
  `shenfenzheng` varchar(18) NOT NULL,
  `shengyuandi` varchar(20) NOT NULL,
  `hukousuozaidi` varchar(50) NOT NULL,
  `hukouleibie` varchar(2) NOT NULL,
  `shentiqingkuang` varchar(50) NOT NULL,
  `shouji` varchar(11) NOT NULL
  `mima` varchar(20) DEFAULT NULL,
  `weixinhao` varchar(20) DEFAULT NULL,
 * 
 * 
 * */
@Data
public class Student {
	
	private String xuehao;
	private String banjihao;
	private String shifoupinkunsheng;
	private String zhengzhimianmao;
	private char shifouxiaowaizhusu;
	private String xuejizhuangtai;
	private String xingming;
	private char xingbie;
	private Date chushengriqi;
	private String shenfenzheng;
	private String shengyuandi;
	private String hukousuozaidi;
	private String hukouleibie;
	private String shentiqingkuang;
	private String shouji;
	private String mima;
	private String weixinhao;
	
	public String getXuehao() {
		return xuehao;
	}
	public void setXuehao(String xuehao) {
		this.xuehao = xuehao;
	}
	public String getBanjihao() {
		return banjihao;
	}
	public void setBanjihao(String banjihao) {
		this.banjihao = banjihao;
	}
	public String getShifoupinkunsheng() {
		return shifoupinkunsheng;
	}
	public void setShifoupinkunsheng(String shifoupinkunsheng) {
		this.shifoupinkunsheng = shifoupinkunsheng;
	}
	public String getZhengzhimianmao() {
		return zhengzhimianmao;
	}
	public void setZhengzhimianmao(String zhengzhimianmao) {
		this.zhengzhimianmao = zhengzhimianmao;
	}
	public char getShifouxiaowaizhusu() {
		return shifouxiaowaizhusu;
	}
	public void setShifouxiaowaizhusu(char shifouxiaowaizhusu) {
		this.shifouxiaowaizhusu = shifouxiaowaizhusu;
	}
	public String getXuejizhuangtai() {
		return xuejizhuangtai;
	}
	public void setXuejizhuangtai(String xuejizhuangtai) {
		this.xuejizhuangtai = xuejizhuangtai;
	}
	public String getXingming() {
		return xingming;
	}
	public void setXingming(String xingming) {
		this.xingming = xingming;
	}
	public char getXingbie() {
		return xingbie;
	}
	public void setXingbie(char xingbie) {
		this.xingbie = xingbie;
	}
	public Date getChushengriqi() {
		return chushengriqi;
	}
	public void setChushengriqi(Date chushengriqi) {
		this.chushengriqi = chushengriqi;
	}
	public String getShenfenzheng() {
		return shenfenzheng;
	}
	public void setShenfenzheng(String shenfenzheng) {
		this.shenfenzheng = shenfenzheng;
	}
	public String getShengyuandi() {
		return shengyuandi;
	}
	public void setShengyuandi(String shengyuandi) {
		this.shengyuandi = shengyuandi;
	}
	public String getHukousuozaidi() {
		return hukousuozaidi;
	}
	public void setHukousuozaidi(String hukousuozaidi) {
		this.hukousuozaidi = hukousuozaidi;
	}
	public String getHukouleibie() {
		return hukouleibie;
	}
	public void setHukouleibie(String hukouleibie) {
		this.hukouleibie = hukouleibie;
	}
	public String getShentiqingkuang() {
		return shentiqingkuang;
	}
	public void setShentiqingkuang(String shentiqingkuang) {
		this.shentiqingkuang = shentiqingkuang;
	}
	public String getShouji() {
		return shouji;
	}
	public void setShouji(String shouji) {
		this.shouji = shouji;
	}
	public String getMima() {
		return mima;
	}
	public void setMima(String mima) {
		this.mima = mima;
	}
	public String getWeixinhao() {
		return weixinhao;
	}
	public void setWeixinhao(String weixinhao) {
		this.weixinhao = weixinhao;
	}
	public String toString() {
		return "Student [xuehao=" + xuehao + ", xingming=" + xingming + "]";
	}
	
	
	
}
