package com.umis.pojo;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * @author: 钱曹宇
 * @date: 2019/4/1 11:28 PM
 * @description:
 */
@Data
public class TemplateData {
    /** 文字的内容 */
    private String value;
    /** 文字的颜色 */
    private String color;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String text) {
        this.value = text;
    }
}
