package com.umis.pojo;

import lombok.Data;

/*
CREATE TABLE `course` (

  `id` int(11) NOT NULL AUTO_INCREMENT,
  `access_token` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
)
*/


/**
 * @author: 钱曹宇
 * @date: 2019/4/2 3:56 PM
 * @description: 微信 access_token
 */
@Data
public class AccessToken {
    private String id;
    private String accessToken;
    private String createTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }
}
