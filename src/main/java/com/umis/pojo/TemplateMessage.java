package com.umis.pojo;

import lombok.Data;

import java.util.Map;

/**
 * @author: 钱曹宇
 * @date: 2019/4/1 11:07 PM
 * @description: 微信模版消息的实体类
 */
@Data
public class TemplateMessage {


    /** 用户的touser（openid） */
    private String touser;

    /** 模版消息的template_id */
    private String template_id;

    /** 用户点击消息框跳转的url，若为空则点击无法跳转 */
    private String url;

    /** 标题颜色 */
    private String title_color;

    /** 内容 */
    private Map<String,TemplateData> data;

    public String getTouser() {
        return touser;
    }

    public void setTouser(String touser) {
        this.touser = touser;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTemplate_id() {
        return template_id;
    }

    public void setTemplate_id(String template_id) {
        this.template_id = template_id;
    }

    public String getTitle_color() {
        return title_color;
    }

    public void setTitle_color(String title_color) {
        this.title_color = title_color;
    }

    public Map<String, TemplateData> getData() {
        return data;
    }

    public void setData(Map<String, TemplateData> data) {
        this.data = data;
    }
}
