package com.umis.shiro;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;

import com.umis.pojo.Student;
import com.umis.service.StudentServiceImpl;


public class StudentRealm extends AuthorizingRealm{

	@Autowired
	private StudentServiceImpl studentService;
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
		
		return null;
	}

	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
		
		UsernamePasswordToken uptoken = (UsernamePasswordToken)token;
		Student student = studentService.findByXuehao(uptoken.getUsername());
		System.out.println(student);
		if(student == null) {
			return null;
		}
		
		return  new SimpleAuthenticationInfo(student,student.getMima(),"");
	}
	
	
}
