package com.umis;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.umis.pojo.TemplateData;
import com.umis.pojo.TemplateMessage;
import com.umis.service.SendMessageService;
import com.umis.service.SendMessageServiceImpl;
import com.umis.utils.WeixinUtils;
import org.apache.shiro.util.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UmisTestApplicationTests {

	@Autowired
	private SendMessageServiceImpl sendMessageService;

	@Test
	public void contextLoads() {
	}
	@Test
	public void getToken(){
		try {
			String result = WeixinUtils.getAccessToken("wxbc7b7ffb371c2ad1","e4874ab0747c7b87afbcd6cfabc6e3e7");
			System.out.println(result);
		}catch (Exception e){
			e.printStackTrace();
		}
	}
	@Test
	public void sendMessage(){


		try {
			int result = sendMessageService.sendTemplateMessage("wxbc7b7ffb371c2ad1","e4874ab0747c7b87afbcd6cfabc6e3e7","oyKTH1KwuWgZx8G0a8TYTtfObo8A");
			System.out.println(result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
